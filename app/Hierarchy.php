<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hierarchy extends Model
{

    protected $table = 'employees';
    protected $fillable = ['name', 'position', 'start', 'salary', 'image', 'id_parent', 'updated_at', 'created_at'];

}
