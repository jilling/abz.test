<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class DatepickerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->publishes([
            __DIR__ . '/../../vendor/eternicode/bootstrap-datepicker/dist' => public_path('datepicker'),
        ], 'public');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
