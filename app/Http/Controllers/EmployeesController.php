<?php

namespace App\Http\Controllers;

use App\Hierarchy;
use Illuminate\Http\Request;
use Validator;
use Session;
use Illuminate\Support\Facades\Redirect;

class EmployeesController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        // выбрать всех с пагинацией

        $data = \App\Hierarchy::paginate(20);

        return view('employees', ["data"=>$data]);
    }

    public function treeEmployees() {

        // Выбрать всех до 2 уровня и отсортировать
        $data = \App\Hierarchy::where('position', '<=', 2)
            ->orderBy('position', 'asc')
            ->get();

        return view('tree-employees', ['data'=>$data]);
    }

    public function sortByField (Request $request) {

        $cntElements = 20;

        //делаем выборку из пришедших данных с пагинацией
        if ( strlen($request->input('search')) > 0 ) {
            $data = \App\Hierarchy::where(
                $request->input('searchBy'), 'LIKE', '%'.$request->input('search').'%')
                ->orderBy($request->input('sort'), $request->input('index'))
                ->take($cntElements)
                ->skip( $cntElements * ( $request->input('page') - 1) )
                ->get();
        } else {
            $data = \App\Hierarchy::orderBy($request->input('sort'), $request->input('index'))
                ->take($cntElements)
                ->skip( $cntElements * ( $request->input('page') - 1) )
                ->get();
        }

        return view('ajax-sort', ['data'=>$data]);
    }

    public function refreshPagination (Request $request) {

        // кол-во элементов в пагинации
        $cntElements = 20;

        if ( strlen($request->input('search')) > 0 ) {
            $data = \App\Hierarchy::where(
                $request->input('searchBy'), 'LIKE', '%'.$request->input('search').'%')
                ->orderBy($request->input('sort'), $request->input('index'))
                ->take($cntElements)
                ->skip( $cntElements * ( $request->input('page') - 1) )
                ->paginate($cntElements);
        } else {
            $data = \App\Hierarchy::orderBy($request->input('sort'), $request->input('index'))
                ->take($cntElements)->skip( $cntElements * ( $request->input('page') - 1) )
                ->paginate($cntElements);
        }

        return $data->links('vendor.pagination.bootstrap-4');
    }

    public function getHierarchy (Request $request) {

        $data = \App\Hierarchy::where('id_parent', '=', $request->input('id'))
            ->orderBy('name', 'asc')
            ->get();

        return view('ajax-hierarchy', ['data'=>$data]);
    }

    public function create() {

        $data = \App\Hierarchy::where('position', '<', '5')
            ->orderBy('position', 'asc')
            ->get();

        return view('employees-create', ["data"=>$data]);

    }

    public function store (Request $request) {

        $rules = array(
            'name'       => 'required',
            'start'       => 'required',
            'salary'      => 'required|numeric',
            'id_parent' => 'required|numeric',
            'image' => 'image'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return \Redirect::to('employees/create')
                ->withErrors($validator);
        } else {

            $path = '';
            if($request->hasFile('image')) {
                $file = $request->file('image');
                $file->move(public_path().'/images', $file->getClientOriginalName());
                $path = "/images/".$file->getClientOriginalName();
            }

            $employee = new Hierarchy;
            $employee->name = $request->input('name');
            $employee->salary = $request->input('salary');
            $employee->position = $request->input('position');
            $employee->start = $request->input('start');
            $employee->id_parent = $request->input('id_parent');
            $employee->image = $path; //Input::get('image') ? Input::get('image') : ''
            $employee->save();

            Session::flash('message', 'Successfully created employees!');
            return Redirect::to('employees/create');
        }
    }

    public function edit ($id) {

        $select = \App\Hierarchy::where('position', '<', '5')
            ->orderBy('position', 'asc')
            ->get();

        $data = Hierarchy::find($id);
        return view('employees-edit', ['data'=>$data, 'select'=>$select]);
    }

    public function update (Request $request) {
        $rules = array(
            'name'       => 'required',
            'start'       => 'required',
            'salary'      => 'required|numeric',
            'id_parent' => 'required|numeric',
            'image' => 'image'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return \Redirect::to('employees/'.$request->input("id").'/edit')
                ->withErrors($validator);
        } else {

            $path = '';

            if($request->hasFile('image')) {
                $file = $request->file('image');
                $file->move(public_path().'/images', $file->getClientOriginalName());
                $path = "/images/".$file->getClientOriginalName();
            } else {
                if (!empty($request->input('image-path'))) {
                    $path = $request->input('image-path');
                }
            }

            $employee = \App\Hierarchy::find($request->input('id'));
            $employee->name = $request->input('name');
            $employee->salary = $request->input('salary');
            $employee->position = $request->input('position');
            $employee->start = $request->input('start');
            $employee->id_parent = $request->input('id_parent');
            $employee->image = $path;
            $employee->save();

            $text = '';
            if ($request->input('page') == 'create') {
                $text = 'Successfully created employees!';
            } elseif ($request->input('page') == 'edit') {
                $text = 'Successfully edited employee with id='.$request->input("id");
            }
            Session::flash('message', $text);
            return Redirect::to('employees');
        }
    }

    public function destroy (Request $request) {
        $result = \App\Hierarchy::destroy($request->input('id'));
        return $result;
    }
}
