;
function Sorting() {
    this.sortIndex = '';
    this.sortBy = '';
    this.numPage = '';
    this.searchBy = '';
    this.searchText = '';
}

Sorting.prototype = {
    getData: function () { // получаем начальные данные

        if ($('.arrow-top').length > 0) {
            this.sortIndex = 'asc';
            this.sortBy = $('.arrow-top').parent().find('.item-text').text();
        } else {
            this.sortIndex = 'desc';
            this.sortBy = $('.arrow-bottom').parent().find('.item-text').text();
        }

        this.numPage = $('.pagination .active').children().text();
        this.searchBy = $('#search-select').val();
        this.searchText = $('#search').val();
    },
    sortData: function () { // обновляем данные с условиями сортировки
        $.ajax({
            type: "POST",
            url: '/sort',
            data: { index: this.sortIndex, sort: this.sortBy, page: this.numPage, search: this.searchText, searchBy: this.searchBy },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function( data ) {
                // очистить user-data и записать новые данные
                $('.users-data').html(data);
            }
        });
    },
    sortPagination: function () { // обновляем пагинацию
        $.ajax({
            type: "POST",
            url: '/employees',
            data: { index: this.sortIndex, sort: this.sortBy, page: this.numPage, search: this.searchText, searchBy: this.searchBy },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function( data ) {
                $('.user-pagination').html(data);
            }
        });
    }
};

var sorting = new Sorting();

$( document ).ready(function() {
    $('#start').datepicker({
        format: 'yyyy-mm-dd'
    });

    // сортировка по полю
    $('.item-text').on('click', function () {

        var isExist = false;

        sorting.getData();
        sorting.sortBy = $(this).text();

        // есть ли уже сортировка на этом столбце
        if ($(this).parent().find('.arrow-bottom').length > 0 || $(this).parent().find('.arrow-top').length > 0) {
            isExist = true;
        } else {
            isExist = false;
        }

        $('.arrow-top').remove();
        $('.arrow-bottom').remove();

        if (!isExist) {
            $(this).after(' <div class="arrow-top"></div>');
            sorting.sortIndex = 'asc';
        } else {
            if (sorting.sortIndex == 'asc') {
                $(this).after(' <div class="arrow-bottom"></div>');
                sorting.sortIndex = 'desc';
            } else {
                $(this).after(' <div class="arrow-top"></div>');
                sorting.sortIndex = 'asc';
            }
        }

        // обновляем данные
        sorting.sortData();
    });

    // поиск
    $('#search').on('keyup', function () {

        // получаем данные
        sorting.getData();

        // обновляем данные
        sorting.sortData();

        // обновляем пагинацию
        sorting.sortPagination();
    });

    $('#id_parent').on('change', function () {
        $('#position').val($('#id_parent :selected').text().split('--')[0]);
    });

});

// выносим чтобы можно было искать по данным, которые будут динамически подгружены
$(document).on('click', '.pagination a', function (e) {
    e.preventDefault();

    sorting.getData();
    sorting.numPage = $(this).attr('href').match(/page=([^&]+)/)[1];

    // обновляем данные
    sorting.sortData();

    // обновляем пагинацию
    sorting.sortPagination();
});
$(document).on('click', '.delete-item', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var _this = this;

    $.ajax({
        type: "POST",
        url: 'employees/destroy',
        data: { id: id },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function( result ) {
            if (result) {

                // Удаляем строку
                $(_this).closest('.list-group-item').remove();

                // обновляем данные
                sorting.sortData();

                // обновляем пагинацию
                sorting.sortPagination();
            }
        }
    });
});

// открытие новых уровней
$(document).on('click', '.employee', function (e) {

    e.preventDefault();
    _this = this;

    // проверить подгружены ли уже сотрудники.
    // если не подгружены - подгрузить
    // уровень иерархии меньше 5, чтобы лишний раз не делать запрос в базу
    if ($(_this).siblings('.sub-tree-employees').length == 0 && $(_this).attr('data-position') < 5) {
        $.ajax({
            type: "POST",
            url: '/hierarchy',
            data: { id: $(_this).attr('data-id') },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function( data ) {
                $(_this).after(data);
            }
        });
    }
});