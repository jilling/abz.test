<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Hierarchy;
use Illuminate\Support\Facades\DB;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // создать первого

        $boss = new Hierarchy();
        $boss->name = 'Boss';
        $boss->position = '1';
        $boss->image = "";
        $boss->start = Carbon::now();
        $boss->salary = 10000;
        $boss->id_parent = 0;
        $boss->save();

        $this->createCycle([$boss->id], 2, 5);
    }

    public function createCycle ($arBosses, $currentPosition, $endPosition) {

        if ($currentPosition > $endPosition) {
            return false;
        }

        $data = array();

        // кол-во подчиненных у каждого босса (по уровням)
        $arPosition = array(
            "2"=>array(
                "employee" => 3,
                "salary" => 8000,
            ),
            "3"=>array(
                "employee" => 3,
                "salary" => 6000,
            ),
            "4"=>array(
                "employee" => 5,
                "salary" => 4000,
            ),
            "5"=>array(
                "employee" => 15,
                "salary" => 2000,
            )
        );

        // для каждого босса из массива записываем новых сотрудников
        foreach ( $arBosses as $boss) {

            for ($i = 1; $i < $arPosition[$currentPosition]["employee"]; $i++) {

                $data[] = array(
                    "name" => 'Employee #'.$i.' Level '.$currentPosition,
                    "position" => $currentPosition,
                    "start" => Carbon::now(),
                    "image" => "",
                    "salary" => $arPosition[$currentPosition]["salary"],
                    "id_parent" => $boss,
                );

            }

            // записываем всех новых сотрудников для данного босса
            Hierarchy::insert($data);

        }

        // выбрать всех только что добавленных сотрудников для передачи в функцию цикла добавления

        $newList = DB::table('employees')->select('id')->where('position', $currentPosition)->get();

        $newBosses = array();

        foreach ($newList as $value) {
            $newBosses[] = $value->id;
        }

        $currentPosition++;

        $this->createCycle($newBosses, $currentPosition, $endPosition);

    }
}
