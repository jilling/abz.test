<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('/');

Auth::routes();

Route::get('employees', 'EmployeesController@index')->name('employees');
Route::get('employees/create', 'EmployeesController@create')->name('employees/create');
Route::get('employees/{id}/edit', 'EmployeesController@edit');
Route::get('/tree-employees', 'EmployeesController@treeEmployees')->name('tree-employees');
// forms
Route::post('employees/update', 'EmployeesController@update')->name('employees/update');
Route::post('employees/destroy', 'EmployeesController@destroy')->name('employees/destroy');
// ajax
Route::post('/sort', 'EmployeesController@sortByField');
Route::post('/employees', 'EmployeesController@refreshPagination');
Route::post('/hierarchy', 'EmployeesController@getHierarchy');