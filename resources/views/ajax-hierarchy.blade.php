<ul class="sub-tree-employees connectedSortable">
    @foreach ($data as $user)
        <li><a href="#" class="employee" data-id="{{$user->id}}" data-position="{{$user->position}}">{{$user->name}}</a></li>
    @endforeach
</ul>