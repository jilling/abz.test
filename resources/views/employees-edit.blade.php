@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-center">Edit employee</h2>
            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            <form action="{{route('employees/update')}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="page" value="edit">
                <div class="form-group row">
                    <label for="name" class="col-2 col-form-label">Name</label>
                    <div class="col-10">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') ? old('name') : $data->name }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="salary" class="col-2 col-form-label">Salary</label>
                    <div class="col-10">
                        <input id="salary" type="text" class="form-control" name="salary" value="{{ old('salary') ? old('salary') : $data->salary }}" required autofocus>

                        @if ($errors->has('salary'))
                            <span class="help-block">
                                <strong>{{ $errors->first('salary') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="parent" class="col-2 col-form-label">Parent</label>
                    <div class="col-10">

                        <input id="id" type="hidden" class="form-control" name="id" value="{{ $data->id}}">

                        <select id="id_parent" class="form-control" name="id_parent" required>
                            <option value="0">{{'0--Level'}}</option>
                            @foreach($select as $user)
                                <option value="{{$user->id}}" <?=(($user->id == old('id_parent')) || ($user->id == $data->id_parent) ) ? 'selected' : '' ?> >{{$user->position .'--Level---ID--'.$user->id.': '.$user->name}}</option>
                            @endforeach
                        </select>

                        <input type="hidden" id="position" name="position" value="{{$data->position}}">

                        @if ($errors->has('id_parent'))
                            <span class="help-block">
                                <strong>{{ $errors->first('id_parent') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="start" class="col-2 col-form-label">Start</label>
                    <div class="col-10">
                        <input id="start" type="text" class="form-control" name="start" value="{{ old('start') ? old('start') : $data->start }}" required autofocus>

                        @if ($errors->has('start'))
                            <span class="help-block">
                                <strong>{{ $errors->first('start') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="image" class="col-2 col-form-label">Image</label>
                    <div class="col-2">
                        @if(!empty($data->image ))
                            <img src="{{$data->image}}" class="img-fluid">
                            <input type="hidden" name="image-path" value="{{$data->image}}" >
                        @endif
                        <input id="image" type="file" class="form-control" name="image" value="{{ old('image') ? old('image') : $data->image }}" autofocus>

                        @if ($errors->has('image'))
                            <span class="help-block">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-primary">
                            Create
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection