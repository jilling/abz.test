@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @php
                $startDepthLvl = false;
                $i_count = 0;
            @endphp

            <ul class="tree-employees connectedSortable">
                @foreach($data as $user)

                    @if ($user->position == 1)
                        <li class="sub-open"><a href="#" class="employee" data-id="{{$user->id}}" data-position="{{$user->position}}">{{$user->name}}</a>
                    @endif

                    @if($user->position == 2 && !$startDepthLvl)
                        @php
                            $startDepthLvl = true;
                        @endphp
                        <ul class="sub-tree-employees connectedSortable">
                    @endif

                    @if($user->position == 2)
                        <li><a href="#" class="employee" data-id="{{$user->id}}" data-position="{{$user->position}}">{{$user->name}}</a></li>
                    @endif

                    @if($i_count == (count($data) - 1))
                        </ul></li>
                    @endif

                    @php
                        $i_count++;
                    @endphp
                @endforeach
            </ul>

        </div>
    </div>
</div>
@endsection
