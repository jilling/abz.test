@foreach ($data as $user)
    <div class="list-group-item">
        <div class="item-id">{{$user->id}}</div>
        <div class="item-name">{{$user->name}}</div>
        <div class="item-start">{{$user->start}}</div>
        <div class="item-salary">{{$user->salary}}</div>
        <div class="item-id-parent">{{$user->id_parent}}</div>
        <div class="item-image"><img src="{{$user->image}}" class="img-fluid"></div>
        <div class="item-buttons">
            <a href="{{'employees/'.$user->id.'/edit'}}" class="btn btn-primary">Edit</a> <a href="#" class="btn btn-primary delete-item" data-id="{{$user->id}}">Delete</a></div>
    </div>
@endforeach