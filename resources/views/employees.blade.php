@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            <form class="form-inline search-form">
                <input class="form-control" id="search" placeholder="Search for...">
                <select class="custom-select search-item-margin" id="search-select">
                    <option value="id">Id</option>
                    <option value="name" selected>Name</option>
                    <option value="start">Start</option>
                    <option value="salary">Salary</option>
                    <option value="id_parent">Parent id</option>
                </select>
                <a href="{{route('employees/create')}}" class="btn btn-primary">Create new employee</a>
            </form>

            <div class="list-group">

                <div class="list-group-item header-item active">
                    <div class="item-id"><div class="item-text">id</div> <div class="arrow-top"></div></div>
                    <div class="item-name"><div class="item-text">name</div></div>
                    <div class="item-start"><div class="item-text">start</div></div>
                    <div class="item-salary"><div class="item-text">salary</div></div>
                    <div class="item-id-parent"><div class="item-text">id_parent</div></div>
                    <div class="item-image"><div class="item-text">image</div></div>
                    <div class="item-buttons">&nbsp;</div>
                </div>

                <div class="users-data">
                    @foreach ($data as $user)
                        <div class="list-group-item">
                            <div class="item-id">{{$user->id}}</div>
                            <div class="item-name">{{$user->name}}</div>
                            <div class="item-start">{{$user->start}}</div>
                            <div class="item-salary">{{$user->salary}}</div>
                            <div class="item-id-parent">{{$user->id_parent}}</div>
                            <div class="item-image"><img src="{{$user->image}}" class="img-fluid"></div>
                            <div class="item-buttons">
                                <a href="{{'employees/'.$user->id.'/edit'}}" class="btn btn-primary">Edit</a> <a href="#" class="btn btn-primary delete-item" data-id="{{$user->id}}">Delete</a></div>
                        </div>
                    @endforeach
                </div>

                <div class="user-pagination">
                    {{$data->links('vendor.pagination.bootstrap-4')}}
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
